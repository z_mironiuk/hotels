function HotelClass(data, tableId) {
    this.data = data;
    this.tableId = '#' + tableId;
}

HotelClass.prototype.initTable = function() {
    var tableId = this.tableId;
    var data = this.data;
    var columns = this.getColumns(data);

    $(tableId).DataTable({
        data: data,
        columns: columns
    });
}

HotelClass.prototype.getColumns = function (data) {
    if(data == null) {
        return;
    }
    var columns = [];

    Object.keys(data[0]).forEach((column) => {
        columns.push({data: column, title: column.toUpperCase()})
    });

    return columns;
}


/**
 * 
 *  Function adds comment to specific hotel
 * 
 */
function addComment(hotelId, storeRoute) {
    var comment = $('#text').val();
    var hotelId = hotelId;
    var storeRoute = storeRoute;

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: storeRoute,
        method: "POST",
        data: {
            text: comment,
            hotel_id: hotelId
        },
        success: function(response) {
            $('.comments').append('<p>' + response.comment + '</p>');
            $('#text').val('');
        },
        error: function(response) {
            $('#textError').text(response.responseJSON.errors.text);
        }
    });
}