@extends('layouts.app')

@section('pageTitle', trans('menu.title'))

@section('content')
    <div class="container">
        <h3>{{ trans('menu.title') }}</h3>
                <p><a href="{{route('hotel.new')}}">{{ trans('menu.hotel_new') }}</a></p>
                <p><a href="{{route('tour.new')}}">{{ trans('menu.tour_new') }}</a></p>
                <p><a href="{{route('tour.index')}}">{{ trans('menu.tours_all') }}</a></p>
                <p><a href="{{route('hotel.index')}}">{{ trans('menu.hotels_all') }}</a></p>
                <p><a href="{{route('tour.statement')}}">{{ trans('menu.tour_statement') }}</a></p>
                <p><a href="{{route('hotel.statement')}}">{{ trans('menu.hotel_statement') }}</a></p>
                <p><a href="{{route('hotelvue')}}">{{ trans('menu.hotels_all') }} - vue</a></p>
    </div>
@endsection