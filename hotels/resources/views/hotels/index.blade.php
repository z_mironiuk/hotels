@extends('layouts.app')

@section('content')
        <div class="container">
            <h3>{{ __('HOTEL')['LIST'] }}</h3>
                <div class="form-group">
                    <form action="{{route('hotel.search')}}" method="get" class="form-inline">
                        <input type="text" name="name" id="name" class="form-control col-sm-3 mr-sm-3" value = "{{ app('request')->input('name') }}" required/>
                        <button class="btn btn-primary col-sm-1 mr-sm-3"><i class="fa fa-search"></i></button>
                        <a href="{{ URL::route('hotel.index') }}" class="btn btn-danger col-sm-1"><i class="fa fa-close"></i></a>
                    </form>
                </div>
        @foreach($hotels as $hotel)
            <fieldset class="border p-2 mb-3">
                <h4 class="form-label mr-2 mb-2">{{ $hotel['name'] }}</h4>                
                <div class="row no-gutters">
                    <div class="col-sm-2 mr-2">
                        <a href="{{ URL::route('hotel.show', $hotel['id']) }}" class="btn btn-info col-sm-12 mb-2"> {{ __('FORMS')['DETAILS'] }}</a>
                    </div>
                    <div class="col-sm-1 mr-2">
                        <a href="{{ URL::route('hotel.edit', $hotel['id']) }}" class="btn btn-info col-sm-12 mb-2"><i class="fa fa-pencil-square-o"></i></a>
                    </div>
                    <div class="col-sm-1 mr-2">
                        <form action="{{ route('hotel.copy', $hotel['id']) }}" method="post">
                            @csrf
                            <button class="btn btn-info col-sm-12 mb-2"><i class=" fa fa-files-o"></i></button>
                        </form>
                    </div>
                    <div class="col-sm-1 mr-2">
                        <form action="{{ route('hotel.delete', $hotel['id']) }}" method="post">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger col-sm-12 mb-2"><i class="fa fa-trash"></i></button>
                        </form>
                    </div>
                </div>
            </fieldset>
        @endforeach
            {{ $hotels->withQueryString()->links("pagination::bootstrap-4") }}

           
        </div>

 
@endsection

