@extends('layouts.app')

@section('content')

<div class="container">
    <table id="table_id" class="display">
    </table>
</div>

@endsection

@push('scripts')

<script type="text/javascript" src="{{ URL::asset('js/HotelController.js') }}"></script>

<script>
    $(document).ready(function () {
        var data = @json($hotels);

        var dataTable = new HotelClass(data, 'table_id');
        dataTable.initTable();
    })


</script>


@endpush