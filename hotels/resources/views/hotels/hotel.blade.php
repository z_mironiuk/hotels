@extends('layouts.app')

@section('pageTitle', $hotel['name'])

@section('content')
        <div class="container">

            <h3>{{ $hotel->name }}</h3>

            <ul>
                <li>{{ $hotel->city }}</li>
                <li>{{ $hotel->country }}</li>
            </ul>

            <div class="comments">
                <h4>{{__('COMMENT')['COMMENTS']}}</h4>
                @foreach ($hotel['comments'] as $comment)
                    <p>Treść komentarza: {{ $comment['text'] }}</p>
                @endforeach
            </div>

            <div class="form-group">
                <form id="commentForm">
                    @csrf
                    <label class="col-12">Dodaj komentarz:</label>
                    <input class="col-12" type="textarea" name="text" id="text" style="height: 80px; margin-bottom: 10px;"/>
                    <span class="text-danger" id="textError"></span>
                    <button class="btn btn-success savebtn col-12" type="submit">Dodaj komentarz</button>
                </form>
            <div>
        </div>

        



@endsection

@push('scripts')

<script type="text/javascript" src="{{ URL::asset('js/HotelController.js') }}"></script>

    <script>
        $(document).ready(function () {
            $('#commentForm').on('submit', function (e) {
                e.preventDefault();
                var hotelId = @json($hotel['id']);
                var storeRoute = '{{ route("comment.store") }}';
                addComment(hotelId, storeRoute);
            })

        })

    </script>
@endpush
