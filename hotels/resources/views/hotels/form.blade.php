@extends('layouts.app')

@section('content')
<div class="container">
    <form class="form-horizontal" action="{{route('hotel.store')}}" method="post">
        @csrf
        <div class="form-group">
            <label for="name" class="col-sm-4">{{ __('HOTEL')['NAME'] }}</label>
            <div class="col-sm-12">
                <input type="text" name="name" id="name" class="form-control @error('name') is-invalid @enderror" @if(old('name') !=null ) value="{{ old('name') }}" @else @isset($hotel->name)
                value="{{$hotel->name}}"
                @endisset
                @endif
                required/>
                @error('name')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
        </div>
        <div class="form-group">
            <label for="city" class="col-sm-4">{{ __('HOTEL')['CITY'] }}</label>
            <div class="col-sm-12">
                <input type="text" name="city" id="city" class="form-control @error('city') is-invalid @enderror" @if(old('city') !=null ) value="{{ old('city') }}" @else @isset($hotel->city)
                value="{{$hotel->city}}"
                @endisset
                @endif
                required />
                @error('city')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
        </div>
        <div class="form-group">
            <label for="country" class="col-sm-4">{{ __('HOTEL')['COUNTRY'] }}</label>
            <div class="col-sm-12">
                <input type="text" name="country" id="country" class="form-control @error('country') is-invalid @enderror" @if(old('country') !=null ) value="{{ old('country') }}" @else @isset($hotel->country)
                value="{{$hotel->country}}"
                @endisset
                @endif
                required />
                @error('country')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
        </div>
        <div class="form-group col-sm-12">
            <input type="submit" value="{{ __('FORMS')['SUBMIT'] }}" class="form-control" />
        </div>
        @isset($hotel->id)
        <div class="form-group">
            <input type="hidden" value="{{$hotel->id}}" name="id" id="id" class="form-control" />
        </div>
        @endisset
    </form>
</div>




@endsection