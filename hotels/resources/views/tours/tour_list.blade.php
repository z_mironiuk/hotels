@foreach($tours as $tour)
<fieldset class="border p-2 mb-3">
    <h5>{{ trans('tour.tour_id', ['id' => $tour->id]) }} </h5>
    <ul>
        <li>{{ trans('tour.price') }} : ${{ $tour->price }}</li>
        <li>{{ $tour->date_from  }} - {{ $tour->date_to }}</li>
        <li>{{ trans('tour.foods')  }} - {{ App\Models\Tour::$foods[$tour->food] }}</li>
        <li>{{ $tour->hotel->name }} </li>
    </ul>
    <div class="row no-gutters">
        <div class="col-sm-2 mr-2">
            <a href="{{ route('tour.show', $tour->id) }}" class="btn btn-info col-sm-12 mb-2"> {{ __('FORMS')['DETAILS'] }}</a>
        </div>
        <div class="col-sm-1 mr-2">
            <a href="{{ route('tour.edit', $tour->id) }}" class="btn btn-info col-sm-12 mb-2"><i class="fa fa-pencil-square-o"></i></a>
        </div>
        <div class="col-sm-1 mr-2">
            <form action="{{ route('tour.delete', $tour->id) }}" method="post">
                @csrf
                @method('DELETE')
                <button class="btn btn-danger col-sm-12 mb-2"><i class="fa fa-trash"></i></button>
            </form>
        </div>
    </div>
</fieldset>
@endforeach

{{ $tours->links("pagination::bootstrap-4") }}