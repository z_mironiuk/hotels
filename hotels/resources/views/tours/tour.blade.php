@extends('layouts.app')

@section('pageTitle', $tour['hotel']['name'])

@section('content')
{{ Breadcrumbs::render() }}

<div class="container">
    <h3>{{ trans('tour.tour_id', ['id' => $tour->id])}}</h3>
    <ul>
        <li>{{ trans('tour.price') }} : ${{ $tour->price }}</li>
        <li>{{ $tour->date_from  }} - {{ $tour->date_to }}</li>
        <li>{{ trans('tour.foods')  }} - {{ App\Models\Tour::$foods[$tour->food] }}</li>
        <li>{{ $tour->hotel->name }} </li>
        <li>{{ trans('hotel.city') }} - {{$tour->hotel->city }} </li>
        <li>{{ trans('hotel.country') }} - {{$tour->hotel->country }}</li>
    </ul>
</div>

@endsection