@extends('layouts.app')

@section('content')
{{ Breadcrumbs::render() }}
<div class="container">
    <h3>{{ trans('tour.list') }} </h3>
    <div class="form-group">
        <label for="food">{{ trans('tour.foods') }} </label>
        <select name="food" id="food">
            <option value="" selected> {{ trans('tour.food_default') }}</option>
            @foreach ($foods as $key => $food)
            <option value="{{$key}}">{{$food}}</option>
            @endforeach
        </select>
    </div>

    <div id="tours">
        @include('tours.tour_list', ['tours' => $tours])
    </div>
</div>

@endsection

@push('scripts')
<script>
    $(document).ready(function() {

        $('#food').on('change', function() {
            var food = $(this).val();
            var searchRoute = '{{ route("tour.search") }}';

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: searchRoute,
                method: "POST",
                data: {
                    food: food
                },
                success: function(response) {
                    $('#tours').html(response);

                }
            });
        })

        $('.pagination a').bind('click', function (e) {
            e.preventDefault();
            
            var page = $(this).text();
            
            var searchRoute = '{{ route("tour.search") }}';
            console.log('test', page, searchRoute);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: searchRoute,
                method: "POST",
                data: {
                    food : '0',
                    page: page
                },
                success: function(response) {
                    $('#tours').html(response);
                }
            });
        })
        $('#tours').on('click', '.page-item, .pagination a', function (e) {
            e.preventDefault();
            
            var page = $(this).text();
            
            var searchRoute = '{{ route("tour.search") }}';
            console.log('testitem', page, searchRoute);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: searchRoute,
                method: "POST",
                data: {
                    food : '0',
                    page: page
                },
                success: function(response) {
                    $('#tours').html(response);
                }
            });

        })
    });
</script>
@endpush