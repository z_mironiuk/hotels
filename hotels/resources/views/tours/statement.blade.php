@extends('layouts.app')

@section('content')

<div class="container">
    <table id="table_id" class="display">
    </table>
</div>

@endsection

@push('scripts')
<script>
    $(document).ready(function() {

        var data = @json($tours);
        var foods = @json(App\Models\Tour::$foods);
        var tableId = "#" + "table_id";

        var columns = [
            {data: 'id', title: 'ID'},
            {data: 'price', title: 'Cena', render: function ( data, type, row ) {
                return '$' + data;
            }},
            {data: 'date_from', title: 'Data od'},
            {data: 'date_to', title: 'Data do'},
            {data: 'food', title: 'Wyzywienie', render: function ( data, type, row ) {
                return foods[data];
            }},
            {data: 'hotel.name', title: 'Hotel'}
        ]

        $(tableId).DataTable({
            data: data,
            columns: columns
        });
    });
</script>
@endpush