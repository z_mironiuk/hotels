@extends('layouts.app')

@section('content')
        <div class="container">
        <form class="form-horizontal"  action="{{route('tour.store')}}" method="post">
            @csrf
            <div class="form-group">
                <label for="price" class="control-label col-sm-2" >{{ __('TOUR')['PRICE'] }}</label>
                <div class="col-sm-12">
                     <input type="number" name="price" id="price" step="0.01" class="form-control"
                            @if(old('price') != null )
                                value = "{{ old('price') }}"
                            @else
                                @isset($tour->price)
                                    value="{{$tour->price}}"
                                @endisset
                            @endif
                        required/>
                @error('price')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                </div>
            </div>
            
            
            <div class="form-group row no-gutters">
                <div class="form-group col-sm-6">
                <label for="date_from" class="control-label col-sm-12">{{ __('TOUR')['DATE_FROM'] }}</label>
                    <div class="col-sm-12">
                        <input type="date" name="date_from" id="date_from" class="form-control @error('date_from') is-invalid @enderror"
                        @if(old('date_from') != null )
                            value = "{{ old('date_from') }}"
                        @else
                            @isset($tour->date_from)
                                value="{{$tour->date_from}}"
                            @endisset
                        @endif
                       required />
                        @error('date_from')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                
                <div class="form-group col-sm-6">
                <label for="date_to" class="control-label col-sm-12">{{ __('TOUR')['DATE_TO'] }}</label>
                <div class="col-sm-12">
                <input type="date" name="date_to" id="date_to" class="form-control @error('date_to') is-invalid @enderror col-sm-12"
                        @if(old('date_to') != null )
                            value = "{{ old('date_to') }}"
                        @else
                            @isset($tour->date_to)
                                value="{{$tour->date_to}}"
                            @endisset
                        @endif
                       required />
                    @error('date_to')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                </div>
                </div>             
            </div>
                    
            <div class="form-group">
                <label for="hotel_id" class="control-label col-sm-2">{{ __('TOUR')['HOTEL'] }}</label>
                <div class="col-sm-12">
                    <select name="hotel_id" id="hotel_id" class="form-control">
                        @foreach($hotels as $hotel)
                        <option value="{{$hotel->id}}"
                                @isset($tour->hotel_id)
                                    @if ($hotel->id == $tour->hotel_id)
                                    selected
                                    @endif
                            @endisset>{{$hotel->name}}
                                </option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="food" class="control-label col-sm-2" >{{ __('TOUR')['FOOD'] }}</label>
                <div class="col-sm-12">
                    <select name="food" id="food" class="form-control">
                        @foreach($foods as $key => $value)
                        <option value="{{$key}}"
                                @isset($tour->food)
                                    @if ($key == $tour->food)
                                    selected
                                    @endif
                                @endisset>
                               {{$value}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
             @isset($tour->id)
                <div class="form-group">
                <input type="hidden" value="{{$tour->id}}" name="id" id="id" class="form-control"/>
            </div>
            @endisset
            <div class="form-group col-sm-12">
                <input type="submit" value="{{ __('FORMS')['SUBMIT'] }}" class="btn btn-secondary col-sm-12"/>
            </div>
        </form>
            </div>

@endsection

