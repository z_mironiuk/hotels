<?php

return [
    'title' => 'Panel administracyjny',
    'hotel_new' => 'Dodaj nowy hotel',
    'tour_new' => 'Dodaj nową wycieczkę',
    'hotels_all' => 'Lista wszystkich hoteli',
    'tours_all' => 'Lista wszystkich wycieczek',
    'hotel_statement' => 'Zestawienie hoteli',
    'tour_statement' => 'Zestawienie wycieczek'
];