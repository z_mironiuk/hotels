<?php

return [
    'list' => 'Lista wszystkich wycieczek',
    'tour_id' => 'Wycieczka nr :id',
    'price' => 'Cena',
    'foods' => 'Wyżywienie',
    'food_default' => 'Dowolne'
];