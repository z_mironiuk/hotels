<?php

use App\Models\Tour;
use Diglactic\Breadcrumbs\Breadcrumbs;
use Diglactic\Breadcrumbs\Generator as BreadcrumbTrail;
 
Breadcrumbs::for('main', function (BreadcrumbTrail $trail) {
    $trail->push('Strona główna', route('main'));
});

Breadcrumbs::for('tour.index', function (BreadcrumbTrail $trail): void {
    $trail->parent('main');
    $trail->push('Lista wycieczek', route('tour.index'));
});

Breadcrumbs::for('tour.show', function (BreadcrumbTrail $trail, $tour): void {
    $trail->parent('tour.index');
    $trail->push('Wycieczka ' . $tour, route('tour.show', $tour));
});