<?php

use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('main');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::view('/hotelvue', 'hotels.hotelvue')->name('hotelvue');

Route::prefix('hotel')->group(function () {
    Route::name('hotel.')->group(function() {
        Route::get('new', 'HotelController@create')->name('new');
        Route::post('store', 'HotelController@store')->name('store');
        Route::get('index', 'HotelController@index')->name('index');
        Route::get('statement', 'HotelController@statement')->name('statement');
        Route::get('{id}', 'HotelController@show')->name('show');
        Route::get('edit/{id}', 'HotelController@edit')->name('edit');
        Route::delete('{id}', 'HotelController@destroy')->name('delete');
        Route::get('index/search', 'HotelController@search')->name('search');
        Route::post('copy/{id}', 'HotelController@copy')->name('copy');
    });
});

Route::prefix('tour')->group(function () {
    Route::name('tour.')->group(function() {
        Route::get('new', 'TourController@create')->name('new');
        Route::post('store', 'TourController@store')->name('store');
        Route::get('index', 'TourController@index')->name('index');
        Route::get('statement', 'TourController@statement')->name('statement');
        Route::post('search', 'TourController@search')->name('search');
        Route::get('{id}', 'TourController@show')->name('show');
        Route::get('edit/{id}', 'TourController@edit')->name('edit');
        Route::delete('{id}', 'TourController@destroy')->name('delete');
    });
});

Route::prefix('comment')->group(function () {
    Route::post('store', 'CommentController@store')->name('comment.store');
});