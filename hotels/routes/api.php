<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/tokens/create', 'Api\UserController@getUserToken');


Route::middleware('auth:sanctum')->post('/hotel', 'Api\HotelController@store');

Route::get('/hotels', 'Api\HotelController@index');
Route::get('/hotel/{id}', 'Api\HotelController@show');
//Route::post('/hotel', 'Api\HotelController@store');