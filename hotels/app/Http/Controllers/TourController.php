<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Hotel;
use App\Models\Tour;
use App\Http\Requests\TourChangeRequest;
use Illuminate\Pagination\Paginator;

class TourController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tours = Tour::with('hotel')->paginate(Tour::PAGINATION);
        $foods = Tour::$foods;

        return view('tours/index', ['tours' => $tours, 'foods' => $foods]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $hotels = Hotel::all();
        $foods = Tour::$foods;

        return view('tours/form', ['hotels' => $hotels, 'foods' => $foods]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TourChangeRequest $request)
    {
        $request->validated();
        $input = $request->all();
        $id = isset($input['id']) ? $input['id'] : null;
        Tour::createOrUpdate($input, $id);

        return redirect('tour/index')->with('mssg', 'Thanks for your order!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tour = Tour::with('hotel')->get()->find($id);

        return view('tours/tour', ['tour' => $tour]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tour = Tour::find($id);
        $hotels = Hotel::all();
        $foods = Tour::$foods;

        return view('tours/form', ['tour' => $tour, 'hotels' => $hotels, 'foods' => $foods]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Tour::destroy($id);
        return redirect('tour/index');
    }

    /**
     * Function gets data to statement
     * 
     */
    public function statement() {
        $tours = Tour::with('hotel')->get()->toArray();

        return view('tours/statement', ['tours' => $tours]);
    }

    /**
     * Function returns filtered data
     * 
     * @param  \Illuminate\Http\Request  $request
     */
    public function search(Request $request) {
        $input = $request->all();
        $food = $input['food'];
        $page = isset($input['page']) ? $input['page'] : 0;

        Paginator::currentPageResolver(function () use ($page) {
            return $page;
        });

        $tours = Tour::with('hotel');

        if(!empty($food)) {
            $tours->where('food', $food);
        }
        $tours = $tours->paginate(Tour::PAGINATION);

        return view('tours/tour_list', ['tours' => $tours]);
    }
}
