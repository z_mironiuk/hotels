<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Hotel;
use App\Http\Requests\HotelChangeRequest;

class HotelController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');   
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $hotels = Hotel::paginate(Hotel::PAGINATION);
        return view('hotels/index', ['hotels' => $hotels]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('hotels/form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(HotelChangeRequest $request)
    {
        $request->validated();
        $input = $request->all();
        $id = isset($input['id']) ? $input['id'] : null;
        Hotel::createOrUpdate($input, $id);
        return redirect('hotel/index');
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //dd(Hotel::with('comments')->get()->find($id)->toArray());
        $hotel = Hotel::with('comments')->get()->find($id);
        //dd($hotel);
        return view('hotels/hotel', ['hotel' => $hotel]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $hotel = Hotel::find($id);
        return view('hotels/form', ['hotel' => $hotel]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Hotel::destroy($id);
        return redirect('hotel/index');
    }
    
    /**
     * Search hotel by name
     *
     * @param  str  $name
     * @return \Illuminate\Http\Response
     */
    public function search()
    {
        $name = request('name');

        $hotels = Hotel::where('name', 'like', '%'.$name.'%')->paginate(Hotel::PAGINATION);
        return view('hotels/index', ['hotels' => $hotels]);
    }
    
    /**
     * Copy hotel
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function copy($id)
    {
        //$hotel = Hotel::find($id)->toArray()->pluck('name');
        //$hotel = Hotel::select('name', 'country', 'city')->where('id', $id)->get()->toArray();
        $hotel = Hotel::find($id)->toArray();
        unset($hotel['id']);
        Hotel::create($hotel);
        return redirect('hotel/index');
    }


    public function statement() {
        $hotels = Hotel::all()->toArray();

        //dd($tours);
        return view('hotels/statement', ['hotels' => $hotels]);
    }
}
