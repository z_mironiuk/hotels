<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Hotel;
use App\Http\Requests\HotelChangeRequest;

class HotelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\Get(
     *   path="/api/hotels",
     *   summary="Lista wszystkich hoteli",
     *   @OA\Response(
     *     response=200,
     *     description="Wszystkie hotele"
     *   ),
     *   @OA\Response(
     *     response="default",
     *     description="Błąd"
     *   )
     * )
     */
    public function index()
    {
        $hotels = Hotel::all();
        $success = true;
        return response()->json(compact('success', 'hotels'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(HotelChangeRequest $request)
    {
        $request->validated();
        $input = $request->all();
        Hotel::create($input);
        
        $success = true;
        $message = 'Pomyślnie dodano hotel';
        return response()->json(compact('success', 'message'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\Get(
     *   path="/api/hotel/{id}",
     *   summary="Wybrany hotel",
     *   @OA\Response(
     *     response=200,
     *     description="Wybrany hotel"
     *   ),
     * @OA\Parameter(
     *    description="Hotel id",
     *    in="path",
     *    name="id",
     *    required=true,
     *    @OA\Schema(
     *       type="integer",
     *       format="int64"
     *    )
     * ),
     *   @OA\Response(
     *     response="default",
     *     description="Błąd"
     *   )
     * )
     */
    public function show($id)
    {
        $success = true;
        $hotel = Hotel::find($id);
        if (empty($hotel))
        {
            $success = false;
            $error = 'Hotel nie został znaleziony';
        }  
        
        return response()->json(compact('success', 'hotel'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
