<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;

class UserController extends Controller
{
    public function getUserToken(Request $request) {
        $input = $request->all();
        $id = isset($input['id']) ? $input['id'] : null;
        $user = User::find($id);
        if(empty($user)) {
            $success = false;
            $error = __('MESSAGES')['USER_NOT_FOUND'];
            return response()->json(compact('success', 'error'));
        }
        $token = $user->createToken('User token')->plainTextToken;
        
        $success = true;
        return response()->json(compact('success', 'token'));
        
    }
}
