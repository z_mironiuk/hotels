<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TourChangeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'price' => 'required',
            'date_from' => 'required|before:date_to',
            'date_to' => 'required|after:date_from',
            'food' => 'required',
            'hotel_id' => 'required',
            
        ];
    }
}
