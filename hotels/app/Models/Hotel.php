<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Dyrynda\Database\Support\CascadeSoftDeletes;
use App\Traits\ModelTrait;

class Hotel extends Model
{
    use HasFactory,
        softDeletes,
        ModelTrait,
        CascadeSoftDeletes;
    
    const PAGINATION = 8;

    protected $table = 'hotels';
    protected $hidden = ['created_at', 'deleted_at', 'updated_at'];
    protected $cascadeDeletes = ['tours', 'comments'];
    protected $fillable = [
        'name',
        'country',
        'city'
    ];
    
    public function tours()
    {
        return $this->hasMany(Tour::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }
}
