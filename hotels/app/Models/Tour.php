<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\ModelTrait;

class Tour extends Model
{
    use HasFactory,
        SoftDeletes,
        ModelTrait;
    
    const OWN_SCOPE = 1;
    const BREAKFAST = 2;
    const BREAKFAST_AND_DINNER = 3;
    const ALL_INCLUSIVE = 4;

    const PAGINATION = 4;
    
    public static $foods = [
        self::OWN_SCOPE => 'Własny zakres',
        self::BREAKFAST => 'Śniadania',
        self::BREAKFAST_AND_DINNER => 'Śniadania i kolacje',
        self::ALL_INCLUSIVE => 'All inclusive'
    ];
    
    protected $table = 'tours';
    //protected $hidden = ['updated_at', 'deleted_at'];
    protected $fillable = [
        'price',
        'date_from',
        'date_to',
        'food',
        'hotel_id',
    ];
    protected $sortable = ['date_from', 'date_to'];
    
    public function hotel()
    {
        return $this->belongsTo('App\Models\Hotel');
    }
}
