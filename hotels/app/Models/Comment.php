<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comment extends Model
{
    use HasFactory,
        softDeletes;

    protected $table = 'comments';
    protected $hidden = ['created_at', 'deleted_at', 'updated_at'];

    protected $fillable = [
        'text',
        'hotel_id'
    ];
}
